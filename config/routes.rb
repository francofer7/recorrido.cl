Rails.application.routes.draw do
  
  get 'availabilities/search_availability', to: 'availabilities#search_availability'
  get 'availabilities/result_search', to: 'availabilities#result_search'

  get 'turns/search_turn', to: 'turns#search_turn'
  get 'turns/result_search', to: 'turns#result_search'

  resources :availability_engineers
  resources :turns
  resources :availabilities
  resources :calendars
  resources :services
  resources :engineers
  resources :schedules
  root 'pages#home'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  

end
