## README

MaaS (Monitoring as a Service) is an system that allows coordinate duty shifts for each monitored service.

## Prerequisites

- Ruby 2.7.4p191
- Rails 6.1.4.4

## Installation

clone

git clone git@gitlab.com:francofer7/recorrido.cl.git


## install dependencies

bundle install

## Create database

rails db:create

## Migrate database

rails db:migrate

## import records

Import Users, Engineers, Turns, Schedules, Calendars, Availabilities records
rails db:seed

## run

rails s
