json.extract! availability, :id, :date, :hs, :canceled, :service_id, :engineer_id, :created_at, :updated_at
json.url availability_url(availability, format: :json)
