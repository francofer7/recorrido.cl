json.extract! availability_engineer, :id, :availability_id, :engineer_id, :created_at, :updated_at
json.url availability_engineer_url(availability_engineer, format: :json)
