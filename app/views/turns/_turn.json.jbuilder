json.extract! turn, :id, :date, :hs, :canceled, :service_id, :engineer_id, :created_at, :updated_at
json.url turn_url(turn, format: :json)
