json.extract! schedule, :id, :day, :hs_start, :hs_end, :description, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
