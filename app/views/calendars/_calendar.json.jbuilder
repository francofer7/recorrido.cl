json.extract! calendar, :id, :validity_start, :validity_end, :duration_turn, :canceled, :schedule_id, :service_id, :created_at, :updated_at
json.url calendar_url(calendar, format: :json)
