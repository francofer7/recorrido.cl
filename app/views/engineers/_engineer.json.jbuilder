json.extract! engineer, :id, :name, :user_id, :created_at, :updated_at
json.url engineer_url(engineer, format: :json)
