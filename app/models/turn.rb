class Turn < ApplicationRecord
  belongs_to :service
  belongs_to :engineer

  validates_uniqueness_of  :engineer_id, scope: [:service_id, :hs, :date]

  # Selecciono los turnos ocupados de un prestador en la fecha seleccionada.
  def self.turns_occupied(service, date_from, engineer)
    Availability.where('service_id = ? AND date = ? AND engineer_id = ? AND canceled = ?', service, date_from, engineer, "false").pluck(:hs)
  end

end
