class Calendar < ApplicationRecord
  belongs_to :schedule
  belongs_to :service

  def self.turns_availables(service, day, date_from, date_to)
    Calendar.joins(:schedule).where("service_id=? AND schedules.day=? AND validity_start <= ? AND validity_end >= ?","#{service}","#{day}","#{date_from}","#{date_to}")
  end

end
