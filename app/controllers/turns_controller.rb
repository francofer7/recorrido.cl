class TurnsController < ApplicationController
  before_action :set_turn, only: %i[ show edit update destroy ]

  # GET /turns or /turns.json
  def index
    @turns = Turn.all
  end

  # GET /turns/1 or /turns/1.json
  def show
  end

  # GET /turns/new
  def new
    #@turn = Turn.new
    @date_from = params[:date_from]
    @date_to = params[:date_to]
    @service = params[:service]
    @turns = Availability.where('service_id = ? AND date >= ? AND date <= ?', @service, @date_from, @date_to).order(date: :asc)

    @turns.each do |turn|
      turn_add = Turn.where('date = ? AND hs = ? AND service_id = ?', turn.date, turn.hs, turn.service_id)
      if turn_add.present?
      else
      repeat_turns = Availability.where('date = ? AND hs = ? AND service_id = ? AND engineer_id <> ?', turn.date, turn.hs, turn.service_id, turn.engineer_id)
      cant_turns_avai = AvailabilityEngineer.joins(:availability).where('date >= ? AND date <= ? AND service_id = ?', @date_from, @date_to, @service).distinct.count(:availability_id)
      #cant_turns_avai = Availability.select('date, hs, service_id').distinct.where('date >= ? AND date <= ? AND service_id = ?', @date_from, @date_to, @service)
      #cant_turns_avai = AvailabilityEngineer.select('id').distinct


      if repeat_turns.present?
        # selecciono el primer turno que se genero entre todos los engineers (el primero que lo asignó)
        first_turn_add = Availability.where('date = ? AND hs = ? AND service_id = ?', turn.date, turn.hs, turn.service_id).order(created_at: :asc).first
        # cantidad turnos asignados al engineer que viene iterando
        cant = Turn.where('date >= ? AND date <= ? AND service_id = ? AND engineer_id = ?',@date_from, @date_to, @service, turn.engineer_id).count
        cant_engineer = Turn.where('date >= ? AND date <= ? AND service_id = ?',@date_from, @date_to, @service).distinct.count(:engineer_id)
        limit_turns = cant_turns_avai / 3
        #ningun enginer puede trabajar mas de 18 turnos por semana (busco si hay segundo ingeniero con horarios disponibles para trabajar)
        if cant >= limit_turns
          second_turn_add = Availability.where('date = ? AND hs = ? AND service_id = ?', turn.date, turn.hs, turn.service_id).order(created_at: :asc).second
          # cantidad turnos asignados al engineer que viene iterando
          cant = Turn.where('date >= ? AND date <= ? AND service_id = ? AND engineer_id = ?',@date_from, @date_to, @service, second_turn_add.engineer_id).count
          # si el segundo ingeniero ya tiene asignado muchas horas busco un tercero
          if cant >= limit_turns
            third_turn_add = Availability.where('date = ? AND hs = ? AND service_id = ?', turn.date, turn.hs, turn.service_id).order(created_at: :asc).third
            # cantidad turnos asignados al engineer que viene iterando
            cant = Turn.where('date >= ? AND date <= ? AND service_id = ? AND engineer_id = ?',@date_from, @date_to, @service, third_turn_add.engineer_id).count
            # si el segundo ingeniero ya tiene asignado muchas horas busco un tercero
            if cant >= limit_turns
            else
              @turn = Turn.new(:date => third_turn_add.date, :hs => third_turn_add.hs, :canceled => false, :service_id => third_turn_add.service_id, :engineer_id => third_turn_add.engineer_id)
              #UserMailer.with(turn: @turn).turn_summary_email.deliver_now #if Rails.env.production?
              @turn.save!
            end

          else
            @turn = Turn.new(:date => second_turn_add.date, :hs => second_turn_add.hs, :canceled => false, :service_id => second_turn_add.service_id, :engineer_id => second_turn_add.engineer_id)
            #UserMailer.with(turn: @turn).turn_summary_email.deliver_now #if Rails.env.production?
            @turn.save!
          end
        else
          @turn = Turn.new(:date => first_turn_add.date, :hs => first_turn_add.hs, :canceled => false, :service_id => first_turn_add.service_id, :engineer_id => first_turn_add.engineer_id)
          #UserMailer.with(turn: @turn).turn_summary_email.deliver_now #if Rails.env.production?
          @turn.save!
        end

      else
        cant = Turn.where('date >= ? AND date <= ? AND service_id = ? AND engineer_id = ?',@date_from, @date_to, @service, turn.engineer_id).count
        limit_turns = cant_turns_avai / 3
        #ningun enginer puede trabajar mas de 18hs por semana
        if cant >= limit_turns
          
        else
          @turn = Turn.new(:date => turn.date, :hs => turn.hs, :canceled => false, :service_id => turn.service_id, :engineer_id => turn.engineer_id)
          #UserMailer.with(turn: @turn).turn_summary_email.deliver_now #if Rails.env.production?
          @turn.save!
        end
      end
      end
    end
    redirect_to turns_path
  end

  # GET /turns/1/edit
  def edit
  end

  # POST /turns or /turns.json
  def create
    @turn = Turn.new(turn_params)

    respond_to do |format|
      if @turn.save
        format.html { redirect_to turn_url(@turn), notice: "Turn was successfully created." }
        format.json { render :show, status: :created, location: @turn }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @turn.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /turns/1 or /turns/1.json
  def update
    respond_to do |format|
      if @turn.update(turn_params)
        format.html { redirect_to turn_url(@turn), notice: "Turn was successfully updated." }
        format.json { render :show, status: :ok, location: @turn }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @turn.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /turns/1 or /turns/1.json
  def destroy
    @turn.destroy

    respond_to do |format|
      format.html { redirect_to turns_url, notice: "Turn was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def search_turn
    @service = Service.all
  end

  def result_search
    @service = params[:service]
    @date_from = params[:date_from]
    @date_to = params[:date_to]
    @turns = Availability.where('service_id = ? AND date >= ? AND date <= ?', @service, @date_from, @date_to)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_turn
      @turn = Turn.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def turn_params
      params.require(:turn).permit(:date, :hs, :canceled, :service_id, :engineer_id)
    end
end
