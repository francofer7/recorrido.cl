class AvailabilityEngineersController < ApplicationController
  before_action :set_availability_engineer, only: %i[ show edit update destroy ]

  # GET /availability_engineers or /availability_engineers.json
  def index
    @availability_engineers = AvailabilityEngineer.all
  end

  # GET /availability_engineers/1 or /availability_engineers/1.json
  def show
  end

  # GET /availability_engineers/new
  def new
    @availability_engineer = AvailabilityEngineer.new
  end

  # GET /availability_engineers/1/edit
  def edit
  end

  # POST /availability_engineers or /availability_engineers.json
  def create
    @availability_engineer = AvailabilityEngineer.new(availability_engineer_params)

    respond_to do |format|
      if @availability_engineer.save
        format.html { redirect_to availability_engineer_url(@availability_engineer), notice: "Availability engineer was successfully created." }
        format.json { render :show, status: :created, location: @availability_engineer }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @availability_engineer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /availability_engineers/1 or /availability_engineers/1.json
  def update
    respond_to do |format|
      if @availability_engineer.update(availability_engineer_params)
        format.html { redirect_to availability_engineer_url(@availability_engineer), notice: "Availability engineer was successfully updated." }
        format.json { render :show, status: :ok, location: @availability_engineer }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @availability_engineer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /availability_engineers/1 or /availability_engineers/1.json
  def destroy
    @availability_engineer.destroy

    respond_to do |format|
      format.html { redirect_to availability_engineers_url, notice: "Availability engineer was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_availability_engineer
      @availability_engineer = AvailabilityEngineer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def availability_engineer_params
      params.require(:availability_engineer).permit(:availability_id, :engineer_id)
    end
end
