class AvailabilitiesController < ApplicationController
  before_action :set_availability, only: %i[ show edit update destroy ]

  # GET /availabilities or /availabilities.json
  def index
    @availabilities = Availability.all
  end

  # GET /availabilities/1 or /availabilities/1.json
  def show
  end

  # GET /availabilities/new
  def new
    #@availability = Availability.new
    @date_from = params[:date_from]
    @date_to = params[:date_to]

    @user_id = params[:engineer_id]
    @engineer = Engineer.find_by(user_id: @user_id)
    @service = params[:service_id]
    @date_from = params[:fecha]
    @hs = params[:hs]
    
      @turn = Availability.new(:date => @date_from, :hs => @hs, :canceled => false, :service_id => @service, :engineer_id => @engineer.id)
      #UserMailer.with(turn: @turn).turn_summary_email.deliver_now #if Rails.env.production?
      @turn.save!
      @turn_existe = Availability.where(:date => @date_from, :hs => @hs, :canceled => false, :service_id => @service).first
      #inserto tmbien en esta tabla para poder contar distintos
      if @turn_existe
        @abailability_engineer = AvailabilityEngineer.new(:availability_id => @turn_existe.id, :engineer_id => @engineer.id)
        @abailability_engineer.save!
      else
        @abailability_engineer = AvailabilityEngineer.new(:availability_id => @turn.id, :engineer_id => @engineer.id)
        @abailability_engineer.save!
      end

       if @turn.save
          flash[:notice] = "Turno registrado con éxito!"
          redirect_back(fallback_location: root_path)
        else
          flash[:error] = "Error en el registro del turno!"
          redirect_to :back
        end   
  end

  # GET /availabilities/1/edit
  def edit
  end

  # POST /availabilities or /availabilities.json
  def create
   
  end

  # PATCH/PUT /availabilities/1 or /availabilities/1.json
  def update
    respond_to do |format|
      if @availability.update(availability_params)
        format.html { redirect_to availability_url(@availability), notice: "Availability was successfully updated." }
        format.json { render :show, status: :ok, location: @availability }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /availabilities/1 or /availabilities/1.json
  def destroy
    @availability.destroy

    respond_to do |format|
      format.html { redirect_to availabilities_url, notice: "Availability was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def search_availability
    @service = Service.all
  end

  def result_search
    @service = params[:service]
    @date_from = params[:date_from]
    @date_to = params[:date_to]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_availability
      @availability = Availability.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def availability_params
      params.require(:availability).permit(:date, :hs, :canceled, :service_id, :engineer_id)
    end
end
