require "test_helper"

class AbailabilityEngineersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @abailability_engineer = abailability_engineers(:one)
  end

  test "should get index" do
    get abailability_engineers_url
    assert_response :success
  end

  test "should get new" do
    get new_abailability_engineer_url
    assert_response :success
  end

  test "should create abailability_engineer" do
    assert_difference('AbailabilityEngineer.count') do
      post abailability_engineers_url, params: { abailability_engineer: { abailabilities_id: @abailability_engineer.abailabilities_id, engineers_id: @abailability_engineer.engineers_id } }
    end

    assert_redirected_to abailability_engineer_url(AbailabilityEngineer.last)
  end

  test "should show abailability_engineer" do
    get abailability_engineer_url(@abailability_engineer)
    assert_response :success
  end

  test "should get edit" do
    get edit_abailability_engineer_url(@abailability_engineer)
    assert_response :success
  end

  test "should update abailability_engineer" do
    patch abailability_engineer_url(@abailability_engineer), params: { abailability_engineer: { abailabilities_id: @abailability_engineer.abailabilities_id, engineers_id: @abailability_engineer.engineers_id } }
    assert_redirected_to abailability_engineer_url(@abailability_engineer)
  end

  test "should destroy abailability_engineer" do
    assert_difference('AbailabilityEngineer.count', -1) do
      delete abailability_engineer_url(@abailability_engineer)
    end

    assert_redirected_to abailability_engineers_url
  end
end
