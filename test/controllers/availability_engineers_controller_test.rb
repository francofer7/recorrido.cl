require "test_helper"

class AvailabilityEngineersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @availability_engineer = availability_engineers(:one)
  end

  test "should get index" do
    get availability_engineers_url
    assert_response :success
  end

  test "should get new" do
    get new_availability_engineer_url
    assert_response :success
  end

  test "should create availability_engineer" do
    assert_difference('AvailabilityEngineer.count') do
      post availability_engineers_url, params: { availability_engineer: { availability_id: @availability_engineer.availability_id, engineer_id: @availability_engineer.engineer_id } }
    end

    assert_redirected_to availability_engineer_url(AvailabilityEngineer.last)
  end

  test "should show availability_engineer" do
    get availability_engineer_url(@availability_engineer)
    assert_response :success
  end

  test "should get edit" do
    get edit_availability_engineer_url(@availability_engineer)
    assert_response :success
  end

  test "should update availability_engineer" do
    patch availability_engineer_url(@availability_engineer), params: { availability_engineer: { availability_id: @availability_engineer.availability_id, engineer_id: @availability_engineer.engineer_id } }
    assert_redirected_to availability_engineer_url(@availability_engineer)
  end

  test "should destroy availability_engineer" do
    assert_difference('AvailabilityEngineer.count', -1) do
      delete availability_engineer_url(@availability_engineer)
    end

    assert_redirected_to availability_engineers_url
  end
end
