require "application_system_test_case"

class AbailabilityEngineersTest < ApplicationSystemTestCase
  setup do
    @abailability_engineer = abailability_engineers(:one)
  end

  test "visiting the index" do
    visit abailability_engineers_url
    assert_selector "h1", text: "Abailability Engineers"
  end

  test "creating a Abailability engineer" do
    visit abailability_engineers_url
    click_on "New Abailability Engineer"

    fill_in "Abailabilities", with: @abailability_engineer.abailabilities_id
    fill_in "Engineers", with: @abailability_engineer.engineers_id
    click_on "Create Abailability engineer"

    assert_text "Abailability engineer was successfully created"
    click_on "Back"
  end

  test "updating a Abailability engineer" do
    visit abailability_engineers_url
    click_on "Edit", match: :first

    fill_in "Abailabilities", with: @abailability_engineer.abailabilities_id
    fill_in "Engineers", with: @abailability_engineer.engineers_id
    click_on "Update Abailability engineer"

    assert_text "Abailability engineer was successfully updated"
    click_on "Back"
  end

  test "destroying a Abailability engineer" do
    visit abailability_engineers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Abailability engineer was successfully destroyed"
  end
end
