require "application_system_test_case"

class AvailabilityEngineersTest < ApplicationSystemTestCase
  setup do
    @availability_engineer = availability_engineers(:one)
  end

  test "visiting the index" do
    visit availability_engineers_url
    assert_selector "h1", text: "Availability Engineers"
  end

  test "creating a Availability engineer" do
    visit availability_engineers_url
    click_on "New Availability Engineer"

    fill_in "Availability", with: @availability_engineer.availability_id
    fill_in "Engineer", with: @availability_engineer.engineer_id
    click_on "Create Availability engineer"

    assert_text "Availability engineer was successfully created"
    click_on "Back"
  end

  test "updating a Availability engineer" do
    visit availability_engineers_url
    click_on "Edit", match: :first

    fill_in "Availability", with: @availability_engineer.availability_id
    fill_in "Engineer", with: @availability_engineer.engineer_id
    click_on "Update Availability engineer"

    assert_text "Availability engineer was successfully updated"
    click_on "Back"
  end

  test "destroying a Availability engineer" do
    visit availability_engineers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Availability engineer was successfully destroyed"
  end
end
