class CreateSchedules < ActiveRecord::Migration[6.1]
  def change
    create_table :schedules do |t|
      t.string :day
      t.time :hs_start
      t.time :hs_end
      t.string :description

      t.timestamps
    end
  end
end
