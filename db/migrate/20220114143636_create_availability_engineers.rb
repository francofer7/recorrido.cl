class CreateAvailabilityEngineers < ActiveRecord::Migration[6.1]
  def change
    create_table :availability_engineers do |t|
      t.references :availability, null: false, foreign_key: true
      t.references :engineer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
