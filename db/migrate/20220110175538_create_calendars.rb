class CreateCalendars < ActiveRecord::Migration[6.1]
  def change
    create_table :calendars do |t|
      t.date :validity_start
      t.date :validity_end
      t.time :duration_turn
      t.boolean :canceled
      t.references :schedule, null: false, foreign_key: true
      t.references :service, null: false, foreign_key: true

      t.timestamps
    end
  end
end
