# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_14_143636) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "availabilities", force: :cascade do |t|
    t.date "date"
    t.time "hs"
    t.boolean "canceled"
    t.bigint "service_id", null: false
    t.bigint "engineer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["engineer_id"], name: "index_availabilities_on_engineer_id"
    t.index ["service_id"], name: "index_availabilities_on_service_id"
  end

  create_table "availability_engineers", force: :cascade do |t|
    t.bigint "availability_id", null: false
    t.bigint "engineer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["availability_id"], name: "index_availability_engineers_on_availability_id"
    t.index ["engineer_id"], name: "index_availability_engineers_on_engineer_id"
  end

  create_table "calendars", force: :cascade do |t|
    t.date "validity_start"
    t.date "validity_end"
    t.time "duration_turn"
    t.boolean "canceled"
    t.bigint "schedule_id", null: false
    t.bigint "service_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["schedule_id"], name: "index_calendars_on_schedule_id"
    t.index ["service_id"], name: "index_calendars_on_service_id"
  end

  create_table "engineers", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_engineers_on_user_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.string "day"
    t.time "hs_start"
    t.time "hs_end"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "turns", force: :cascade do |t|
    t.date "date"
    t.time "hs"
    t.boolean "canceled"
    t.bigint "service_id", null: false
    t.bigint "engineer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["engineer_id"], name: "index_turns_on_engineer_id"
    t.index ["service_id"], name: "index_turns_on_service_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "admin", default: false, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "availabilities", "engineers"
  add_foreign_key "availabilities", "services"
  add_foreign_key "availability_engineers", "availabilities"
  add_foreign_key "availability_engineers", "engineers"
  add_foreign_key "calendars", "schedules"
  add_foreign_key "calendars", "services"
  add_foreign_key "engineers", "users"
  add_foreign_key "turns", "engineers"
  add_foreign_key "turns", "services"
end
