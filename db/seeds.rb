# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Seeding Database"

# Temporary admin account
unless User.exists?(email: 'admin@gmail.com')
  u = User.new( email: "admin@admin.com",
                password: "123456",
                admin: true)
  u.save!
end

# Temporary engineer and user account for Ernesto
unless User.exists?(email: 'ernesto@gmail.com')
  u = User.new( email: "ernesto@gmail.com",
                password: "123456",
                admin: false)
  u.save!
  e = Engineer.new(name: "Ernesto",
                user_id: u.id)
  e.save!
end

# Temporary engineer and user account for Barbara
unless User.exists?(email: 'barbara@gmail.com')
  u = User.new( email: "barbara@gmail.com",
                password: "123456",
                admin: false)
  u.save!
  e = Engineer.new( name: "Barbara",
                user_id: u.id)
  e.save!
end

# Temporary engineer and user account for Benjamin
unless User.exists?(email: 'benjamin@gmail.com')
  u = User.new( email: "benjamin@gmail.com",
                password: "123456",
                admin: false)
  u.save!
  e = Engineer.new( name: "Benjamin",
                user_id: u.id)
  e.save!
end


## Services whit calendar and schedules

# Temporary engineer and user account for Benjamin
unless Service.exists?(name: 'Servicio Monitoreado 1')
  service = Service.new( name: "Servicio Monitoreado 1")
  service.save!
  s1 = Schedule.new(day: "1",
                hs_start: "2000-01-01 19:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Lunes 19hs a 00hs")
  s1.save!
  c1 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s1.id,
                service_id: service.id)
  c1.save!

  s2 = Schedule.new(day: "2",
                hs_start: "2000-01-01 19:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Martes 19hs a 00hs")
  s2.save!
  c2 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s2.id,
                service_id: service.id)
  c2.save!

  s3 = Schedule.new(day: "3",
                hs_start: "2000-01-01 19:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Miercoles 19hs a 00hs")
  s3.save!
  c3 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s3.id,
                service_id: service.id)
  c3.save!

  s4 = Schedule.new(day: "4",
                hs_start: "2000-01-01 19:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Jueves 19hs a 00hs")
  s4.save!
  c4 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s4.id,
                service_id: service.id)
  c4.save!

  s5 = Schedule.new(day: "5",
                hs_start: "2000-01-01 19:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Viernes 19hs a 00hs")
  s5.save!
  c5 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s5.id,
                service_id: service.id)
  c5.save!

  s6 = Schedule.new(day: "6",
                hs_start: "2000-01-01 10:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Sabado 10hs a 00hs")
  s6.save!
  c6 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s6.id,
                service_id: service.id)
  c6.save!
  s7 = Schedule.new(day: "7",
                hs_start: "2000-01-01 10:00:00.000000000 +0000",
                hs_end: "2000-01-01 00:00:00.000000000 +0000",
                description: "Domingo 10hs a 00hs")
  s7.save!
  c7 = Calendar.new(validity_start: "2022-01-14",
                validity_end: "2023-01-14",
                duration_turn: "2000-01-01 01:00:00.000000000 +0000",
                canceled: false,
                schedule_id: s7.id,
                service_id: service.id)
  c7.save!
end

